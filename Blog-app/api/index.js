const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongooes = require('mongoose');
const authRoute = require('./routes/auth');
const userRoute = require('./routes/users');
const postRoute = require('./routes/posts');
const categoryRoute = require('./routes/category');
const multer = require('multer');
const path = require('path');




// MIDDLEWARE SECTION 
dotenv.config()
app.use(express.json())
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "images")
    }, filename: (req, file, cb) => {
        cb(null, req.body.name)
    }
});
// TO ACCESS THE LOCAL OR STATIC IMAGES OI THE IMAGES PUBBLIC 
app.use("/images", express.static(path.join(__dirname, "/images")))

// MIDDLEWARE SECTION END 



// SERVER NOTICATION RUNNING
mongooes.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,

}).then(function () {
    console.log("CONNECTED TO MONGODB NICE LODDS!! ");
}).catch(err => console.log(err, "ERORR SIYA NEED SIYA FIX!!!!!"));


//ROUTE SECTION REST API
app.use("/api/auth", authRoute);
app.use("/api/user", userRoute);
app.use("/api/post", postRoute);
app.use("/api/category", categoryRoute);

// UPLOAD THE FILE IMAGES LOCALA 
const upload = multer({ storage: storage });

app.post("/api/upload", upload.single("file"), (req, res) => {
    res.status(200).json("File has been uploded")
});



// RUNNING THR SERVER SECTION TO DEFINE RHE CONNECTIOIN OF SERVER
app.listen("8000", () => {
    console.log("Backend is running");
})



