import { createContext, useReducer, useEffect } from "react"
import Reducer from "./Reducer";

// DEFINE THE INITIAL STATE  or DEFAULT STATE
const INITIAL_STATE = {
    // STORE THE DATA IN LOCAL STORE IN WEB SERVER 
    user: JSON.parse(localStorage.getItem("user")) || null,
    isFetching: false,
    error: false,
};


// CREATE  OR DEFINE AS THE MEMORY OR STORE THE STATE OBJECT MUTABLE
// TO SHARE GLOBALLY TO THE COMPONENTS
export const Context = createContext(INITIAL_STATE);


// DEFINE TO FUNCTION  TO PROVIDE SOME COMPONENTS TO THE INDEX.JS 
export const ContetProvider = ({ children }) => {
    // DEFINE THE REDUCER TO MUTATE THE STATE 
    // OPERATION  SECTION FOR LOGIC STATE COMPLEX  
    const [state, dispatch] = useReducer(Reducer, INITIAL_STATE);

    // IF THE STATE ORR DOM IS RENDER SIDE EFFECT WILL BE RENDER 
    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(state.user));
    }, [state.user])


    return (
        <Context.Provider value={{
            user: state.user,
            isFetching: state.isFetching,
            error: state.error,
            dispatch: dispatch,
            // in ES6 YOU CAN USE dispacth if same word 
        }}>
            {/* THIS CHIDREN IS THE WAY TO SHARE THE COMPONENT */}
            {children}

        </Context.Provider>
    )
}
