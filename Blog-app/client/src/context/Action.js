export const LoginStart = (userCredential) => ({
    type: "LOGIN_START"
});


export const LoginSucess = (user) => ({
    type: "LOGIN_SUCCESS",
    payload: user,
});

export const LoginFailure = () => ({
    type: "lOGIN_FAILURE",
})


export const LogOut = () => ({
    type: "LOGOUT",
})



export const UpdateStart = (userCredential) => ({
    type: "UPDATE_START"
});


export const UpdateSucess = (user) => ({
    type: "UPDATE_SUCCESS",
    payload: user,
});

export const UpdateFailure = () => ({
    type: "UPDATE_FAILURE",
})