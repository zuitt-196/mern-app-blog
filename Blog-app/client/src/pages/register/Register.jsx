import axios from 'axios';
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import "./rigister.css"
const Register = () => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(false);


    // DEFINE THE SUBMIT FUNCTION AND FETCHING THE API 
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const res = await axios.post('/auth/register', {
                username,
                email,
                password,
            })

            res.data && window.location.replace("/login")
            console.log(res.data)
        } catch (error) {
            setError(true)

        }
    }

    return (

        <div className="register">
            <span className='register-title'>Register</span>
            <form className="register-form" onSubmit={handleSubmit}>
                <label >Username </label>
                <input type="text" className="register-input" placeholder='Enter your username ..' onChange={e => setUsername(e.target.value)} />

                <label >Email</label>
                <input type="text" className="register-input" placeholder='Enter your email..' onChange={e => setEmail(e.target.value)} />

                <label >Password</label>
                <input type="password" className="register-input" placeholder='Enter your password...' onChange={e => setPassword(e.target.value)} />

                <button className='register-button' type='submit'>Register</button>
                <button className='login-button'>
                    <Link to="/login" className='link'>Login</Link>

                </button>
                {error && <span style={{ color: "red", marginTop: "10px" }}>Something went wrong!</span>}
            </form>
        </div>
    )
}

export default Register
