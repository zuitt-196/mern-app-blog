import React, { useState, useEffect } from 'react'
import Header from '../../components/header/Header'
import Posts from '../../components/posts/Posts'
import Sidebar from '../../components/sidebar/Sidebar'
import './home.css'
import axios from 'axios'
import { useLocation } from 'react-router-dom'
const Home = () => {
    // DIFINE THE STATE with empty array value  since we used later for fetching
    const [posts, setPosts] = useState([]);
    // console.log(posts.length === 0 ? false : true)
    // DEFINE THE LOCATION OBJECT OF BUT  WE NEED TO DEFRACTURE OBJECT TO PICK ONLY THW CERTAIN KEY WHICH IS SEACRH
    const { search } = useLocation();


    useEffect(() => {
        const fetchPosts = async () => {
            // define the API post in backend  use get http method
            // USED OF SEARCH WE CAN USED THE QUERY WITH THW SPECIF DETAILS 
            const res = await axios.get('/post/getAllpost' + search);
            setPosts(res.data);

        }
        fetchPosts();
    }, [axios, search]);

    return (
        <>
            <Header />
            {posts.length === 0 ? (
                <div className="need-upload-post">
                    <h4>NEED TO UPLOAD POST!!!!</h4>
                </div>
            ) :
                <div className='home'>
                    <Posts posts={posts} />
                    <Sidebar />

                </div>
            }
        </>
    )
}

export default Home
