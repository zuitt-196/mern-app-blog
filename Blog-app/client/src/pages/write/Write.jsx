import axios from 'axios';
import React, { useState, useContext } from 'react'
import { Context } from '../../context/Context';
import "./write.css"


const Write = () => {
    // DEFINE THE DEFULT  STATE
    const [title, setTitle] = useState("");
    const [desc, setDesc] = useState("");
    const [file, setFile] = useState(null);
    const { user } = useContext(Context)



    const handlSubmit = async (e) => {
        e.preventDefault();

        // DEFNE THE CREATE THE POST  
        const newPost = {
            username: user.username,
            title,
            desc,

        }
        // UPLOADINF FILE IMAGE 
        if (file) {
            const data = new FormData();
            const filename = Date.now() + file.name;
            // INSERT THE NODE IN FORM HTML ELEMENT WIHT THE PROOTYPE OF append method built in js  
            data.append("name", filename);
            data.append("file", file);
            newPost.photo = filename;

            try {
                await axios.post("/upload", data);
            } catch (error) {
                console.log(error);
            }
        }

        try {
            const res = await axios.post("/post/createPost", newPost);
            console.log("res:", res.data);
            // window.location.replace("/post/" + res.data._id);
            console.log(window.location.replace("/post/" + res.data._id))
        } catch (error) {

            console.log(error);
        }
    }



    return (
        <div className="write">

            {file &&
                (
                    <img className="write-img" src={URL.createObjectURL(file)} alt="write-img" />

                )}

            <form className="write-form" onSubmit={handlSubmit}>
                <div className="write-form-group">
                    <label htmlFor="file-input" className="write-form-label"><i className="write-form-icon fa-sharp fa-solid fa-plus"></i></label>
                    <input type="file" id="file-input" style={{ display: "none" }} onChange={e => setFile(e.target.files[0])} />
                    <input type="text " placeholder="Title" className='write-input' autoFocus={true} onChange={e => setTitle(e.target.value)} />
                </div>

                <div className="write-form-group">
                    <textarea type="text " placeholder="Tell your Story..." className="write-input write-Text" onChange={e => setDesc(e.target.value)}></textarea>
                </div>

                <button className='write-submit' type='submit'>Publish</button>
            </form>
        </div>
    )
}

export default Write
