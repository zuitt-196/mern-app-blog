import React, { useRef, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Context } from '../../context/Context';
import axios from 'axios';
import "./login.css"

const Login = () => {

    // DEFINE THE CURRENT OBJECT VALUE
    const userRef = useRef();
    const passwordRef = useRef();



    const { dispatch, isFetching } = useContext(Context);

// DEFIEN THE HANDLESUBMIT FUNCTION
const handleSumbit = async (e) => {
        e.preventDefault();
        dispatch({ type: "LOGIN_START" });

        try {
            const res = await axios.post("/auth/login", {
                username: userRef.current.value,
                password: passwordRef.current.value,
            })

            dispatch({ type: "LOGIN_SUCCESS", payload: res.data });

        } catch (error) {
            dispatch({ type: "LOGIN_FAILURE" });
            alert("somethig wronng")
        }

    }

    return (
        <div className="login">
            <span className='login-title'>Login</span>
            <form className="login-form" onSubmit={handleSumbit}>
                <label >Username</label>
                <input type="text" className="login-input" placeholder='Enter your username..' ref={userRef} />
                <label >Password</label>
                <input type="password" className="login-input" placeholder='Enter your password...' ref={passwordRef} />
                <button className="loginButton" type='submit' disabled={isFetching}>Login</button>
            </form>
            <button className="rigister-button">
                <Link className="link" to="/register">Register</Link>
            </button>


        </div>
    )
}

export default Login