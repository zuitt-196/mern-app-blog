import React, { useContext, useState } from 'react'
import Sidebar from '../../components/sidebar/Sidebar'
import { Context } from '../../context/Context'
import "./settings.css"
import axios from 'axios';

const Setting = () => {
    // DEFINE THE DEFULT  STATE
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [success, setSucess] = useState(false);
    const [file, setFile] = useState(null);

    const { user, dispatch } = useContext(Context);
    // DEFINE THE PUBLIC FOLDER FOR IMAGE ABPUT ACCESS THE URL IMAFE ON LOCAL

    const PF = "http://localhost:8000/images/";


    const handlSubmit = async (e) => {
        e.preventDefault();
        dispatch({ type: "UPDATE_START" });
        // DEFNE THE CREATE THE POST  
        const upadateUser = {
            userId: user._id,
            username,
            email,
            password,

        }
        // UPLOADINF FILE IMAGE 
        if (file) {
            const data = new FormData();
            const filename = Date.now() + file.name;
            // INSERT THE NODE IN FORM HTML ELEMENT WIHT THE PROOTYPE OF append method built in js  
            data.append("name", filename);
            data.append("file", file);
            upadateUser.profile = filename;

            try {
                await axios.post("/upload", data);
            } catch (error) {
                console.log(error);
            }
        }

        try {
            const res = await axios.put("/user/updateUser/" + user._id, upadateUser);
            setSucess(true)
            dispatch({ type: "UPDATE_SUCCESS", payload: res.data })

        } catch (error) {
            dispatch({ type: "UPDATE_FAILURE" })

        }
    }

    return (
        <div className="settings">
            <div className="setting-wrarpper">
                <div className="setting-title">
                    <span className="setting--update-title">
                        Upate your account
                    </span>
                    <span className="setting--delete-title">
                        Delete Account
                    </span>
                </div>
                <form className='setting-form' onSubmit={handlSubmit}>
                    <label> Profile picture</label>
                    <div className="setting-profile-picture">
                        <img src={file ? URL.createObjectURL(file) : PF + user.profile} alt="" />
                        <label htmlFor="file-input">
                            <i className=" setting-icon fa-solid fa-user"></i>
                        </label>
                        <input type="file" id="file-input" style={{ display: "none" }} onChange={(e) => setFile(e.target.files[0])} />
                    </div>


                    <label>Username</label>
                    <input type="text" placeholder={user.username} onChange={(e) => setUsername(e.target.value)} />
                    <label>Email</label>
                    <input type="email" placeholder={user.email} onChange={(e) => setEmail(e.target.value)} />

                    <label> password</label>
                    <input type="password" onChange={(e) => setPassword(e.target.value)} />
                    <button className='setting-submit' type="submit">Update</button>
                    {success && (<span className='setting-update'>Profile has been updated.....</span>)}
                </form>
            </div>
            <Sidebar />
        </div>
    )
}

export default Setting