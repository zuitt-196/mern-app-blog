import React from 'react';
import "./post.css";
import { Link } from 'react-router-dom';

const Post = ({ post }) => {
  const PF = "http://localhost:8000/images/";

  console.log("post:", PF + post.photo);


  return (


    <div className='post'>
      {post.photo && (
        <img className='post-img' alt="post-1" src={PF + post.photo} />

      )}

      <div className="post-info">
        <div className="post-catergorys">
          {post.categories.map(cat => (
            <span className="post-cat" key={cat._id}>{cat.name}</span>
          ))}

        </div>
        <Link to={`/post/${post._id}`} className="link">
          <span className='post-title'>
            {post.title} </span>
        </Link>

        <hr />
        {/* by getting the date of post but we need convert into datye string */}
        <span className="post-Date">{new Date(post.createdAt).toDateString()}</span>
      </div>
      <p className="post-description">{post.desc}
      </p>
    </div>
  )
}

export default Post