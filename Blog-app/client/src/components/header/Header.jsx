import React from 'react'
import './header.css'
import header from '../../images/header.jpg'

const Header = () => {
    return (
        <div className="header">
            <div className="header-titles">
                <span className="header-title-small">React & Node</span>
                <span className="header-title-large">Blog</span>
            </div>
            <img className="header-img" src={header} alt="header" />
        </div>
    )
}

export default Header