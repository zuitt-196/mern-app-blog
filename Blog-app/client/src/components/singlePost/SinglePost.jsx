import React, { useEffect, useState, useContext } from 'react';
import './singlepost.css';
import { useLocation, Link } from 'react-router-dom';
import axios from 'axios'
import { Context } from '../../context/Context';


const SinglePost = () => {
    // DEFINE THE THE OBJECT URL LOCATION OF ID THEN GET THE WITH USED THE SPLIT FUNCTION IN JS 
    const location = useLocation();
    const path = (location.pathname.split('/')[2]);
    const [post, satPost] = useState({});
    const PF = "http://localhost:8000/images/";

    // DEFINE STATE FOR EDITING 
    const [title, setTitle] = useState("");
    const [desc, setDesc] = useState("");
    const [updateMode, setUpdatedMode] = useState(false);


    const { user } = useContext(Context);

    //    DEFINE THE  DELETE FUNCTION 
    const handleDelete = async (e) => {
        e.preventDefault();
        // example notifactio  foe delete 
        alert("YOU WANT TO DELETE  THIS")
        try {

            await axios.delete(`/post/postDelete/${post._id}`, { data: { username: user.username } });
            window.location.replace("/");
        } catch (error) {
            alert(error.message);
        }
    }
    // DEFIONE THE FUNCTION FOR HANDLE UPDATE 
    const handleUpdate = async () => {
        try {
            await axios.put(`/post/postUpdate/${post._id}`, { username: user.username, title: title, desc: desc });
            setUpdatedMode(false);
        } catch (error) {
            alert(error.message);
        }
    }


    // DEFINE THE SIDE EFFECT TO GET THE API
    useEffect(() => {
        const getPost = async () => {
            const res = await axios.get('/post/getPostById/' + path);
            satPost(res.data);
            setTitle(res.data.title);
            setDesc(res.data.desc);
        }
        getPost();
    }, [axios])

    return (
        <div className="singlepost">
            <div className="single-post-wrap">
                {PF + post.photo &&
                    (<img src={PF + post.photo} alt="img" className="single-post-image" />

                    )}
                {updateMode ? (<input type="text" value={title} className="single-post-title-input" autoFocus onChange={(e) => setTitle(e.target.value)} />) :
                    (
                        <div className="single-post-title-with-edit">
                            <h1 className="single-post-title">
                                {title}
                            </h1>

                            {post.username === user?.username &&
                                (<div className="single-post-edit">
                                    <i className="single-post-icon fa-regular fa-pen-to-square" onClick={() => setUpdatedMode(true)}></i>
                                    <i className="single-post-icon  fa-solid fa-trash" onClick={handleDelete}></i>
                                </div>)
                            }

                        </div>
                    )
                }

                <div className="edit-post-info">
                    <span className="single-post-author">Author: <Link to={`/?user=${post.username}`} className="link"><b>{post.username}</b></Link> </span>
                    <span className="single-post-author">{new Date(post.createdAt).toDateString()}</span>
                </div>
                {updateMode ? (<textarea className="single-post-description-input" value={desc} onChange={(e) => setDesc(e.target.value)} />) : (
                    <p className='single-post-description'>{desc}</p>
                )}

                {updateMode &&
                    <div className='button-container'>
                        <button className="singlePost-Button" onClick={handleUpdate}>
                            Update
                        </button>
                    </div>
                }
            </div>
        </div>

    )
}

export default SinglePost
