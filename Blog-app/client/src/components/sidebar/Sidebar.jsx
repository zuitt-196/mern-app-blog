import React, { useState, useEffect } from 'react'
import './sidebar.css'
import pic1 from '../../images/sidebar-pic-1.jpg';
import { Link } from 'react-router-dom';
import axios from 'axios';
const Sidebar = () => {
    const [cats, setCat] = useState([]);


    useEffect(() => {
        const getCats = async () => {
            const res = await axios.get('/category/getCategory');
            setCat(res.data)
        }
        getCats();
    }, [])


    return (
        <div className="sidebar">
            <div className="sidebar-item">
                <span className="sidebar-title">ABOUT ME</span>
                <img src={pic1} alt="pic-1" />
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti ea, facere facilis exercitationem voluptas ex! Expedita consectetur totam ea nesciunt a hic neque? Ipsa hic laboriosam quas, iste aut non.</p>
            </div>
            <div className="sidebar-item">
                <span className="sidebar-title">CATEGORY</span>
                <ul className='sidebar-list'>

                    {cats.map(cat => (
                        <Link to={`/?cat=${cat.name}`} className="link" key={cat._id}>
                            <li className="sidebar-list-item" >
                                {cat.name}
                            </li>
                        </Link>

                    ))}

                </ul>
            </div>
            <div className="sidebar-item">
                <span className="sidebar-title">FOLLOW US</span>
                <div className="sidebar-social">
                    <i className="sidebar-icon fa-brands fa-facebook"></i>
                    <i className="sidebar-icon fa-brands fa-twitter"></i>
                    <i className="sidebar-icon fa-brands fa-pinterest"></i>
                    <i className=" sidebar-icon fa-brands fa-instagram"></i>
                </div>
            </div>
        </div>
    )
}

export default Sidebar
