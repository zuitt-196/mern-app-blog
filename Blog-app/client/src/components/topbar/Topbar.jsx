import { useContext } from "react";
import "./topbar.css"
import { Link } from "react-router-dom";
import { Context } from "../../context/Context";


const Topbar = () => {
    const { user, dispatch } = useContext(Context)
    const PF = "http://localhost:8000/images/";
    // DEFINE A FUNCTION 
    const handleLogout = () => {
        dispatch({ type: "LOGOUT" })
    }

    return (
        <div className="topbar">
            <div className="top-left">
                <i className="top-icon fa-brands fa-facebook"></i>
                <i className="top-icon fa-brands fa-twitter"></i>
                <i className="top-icon fa-brands fa-pinterest"></i>
                <i className=" top-icon fa-brands fa-instagram"></i>
            </div>
            <div className="top-center">
                <ul className="top-list">
                    <li className="top-list-item">
                        <Link to="/" className="link">
                            HOME
                        </Link>
                    </li>
                    <li className="top-list-item">
                        <Link to="/" className="link">
                            ABOUT
                        </Link>
                    </li>
                    <li className="top-list-item">
                        <Link to="/contact" className="link">
                            CONTACT
                        </Link>
                    </li>
                    <li className="top-list-item">
                        <Link to="/write" className="link">
                            WRITE
                        </Link>
                    </li>
                    <li className="top-list-item" onClick={handleLogout}>
                        {user && "LOGOUT"}
                    </li>
                </ul>
            </div>

            <div className="top-right">

                {user ? (
                    <Link to="/setting">

                        <img className="top-img " src={PF + user.profile} alt="profile" />

                    </Link>) : (

                    <ul className="top-list">
                        <li className="top-list-item">
                            <Link className="link" to="/login"> LOGIN</Link>
                        </li>
                        <li className="top-list-item">
                            <Link className="link" to="/register"> REGISTER</Link>

                        </li>

                    </ul>

                )}

                <i className="top-search-icon fa-solid fa-magnifying-glass"></i>
            </div>
        </div>
    )
}

export default Topbar