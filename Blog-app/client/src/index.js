import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';
import { ContetProvider } from './context/Context';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(

  <ContetProvider>
    <App />
  </ContetProvider>


);


